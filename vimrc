execute pathogen#infect()
set runtimepath^=~/.vim/bundle/ctrlp
filetype plugin indent on
set smarttab autoindent
syntax enable
colorscheme dannzzor
"autocmd vimenter * NERDTree

set backspace=indent,eol,start
set ruler
set colorcolumn=80
set cursorline cursorcolumn
set noswapfile
set encoding=utf-8
set number
let mapleader = ","

" Go Lang
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1

set pastetoggle=<F2>
map <F4> :TagbarToggle<CR>
nmap <F3> :NERDTreeToggle<CR>

let g:ctrlp_custom_ignore = {
	\ 'dir': '\v[\/](.git|.svn|vendor|node_modules)'
	\ }

set hidden

set laststatus=2

" move between buffers in a window
map <leader>m :bnext<CR>
map <leader>n :bprevious<CR>

" close pane
map <leader>x :x<CR>

" close current buffer without closing window
noremap <leader>w :Bdelete<CR>

" open new empty buffer in current window
map <leader>b :ene<CR>

" Move between splits
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

set mouse+=a
if &term =~ '^screen'
	set ttymouse=xterm2
endif

noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

set encoding=utf-8

" NEO CONFIG
let g:neocomplete#enable_at_startup = 1

" disables neocomplete preview pane
set completeopt-=preview

imap <expr><CR> pumvisible() ?
\(neosnippet#expandable() ? "\<Plug>(neosnippet_expand)" : neocomplete#close_popup())
\: "\<CR>"
imap <expr><TAB> neosnippet#jumpable() ?
\ "\<Plug>(neosnippet_jump)"
\: pumvisible() ? "\<C-n>" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif

" Airline CONFIG

let g:airline_theme='dannzzor'

let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1

